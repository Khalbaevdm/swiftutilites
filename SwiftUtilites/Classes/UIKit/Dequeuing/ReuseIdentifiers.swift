//
//  ReuseIdentifiers.swift
// 
//
//  Created by Dmitriy Khalbaev on 02/02/2018.
//

import UIKit

public protocol ReuseIdetifiable: class {
    static func reuseIdentifier() -> String
}

extension ReuseIdetifiable {
    static public func reuseIdentifier() -> String {
        return NSStringFromClass(self)
    }
}

extension UITableViewCell: ReuseIdetifiable { }
extension UICollectionViewCell: ReuseIdetifiable { }
