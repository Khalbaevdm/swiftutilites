//
//  CellDequeuing.swift
//
//
//  Created by Dmitriy Khalbaev on 05/02/2018.
//
//

import Foundation

public typealias ReusableFromNib = NibCompatible & ReuseIdetifiable

public extension UITableView {
    public func registerNib<T: ReusableFromNib>(for cellClass: T.Type) {
        register(cellClass.nib(), forCellReuseIdentifier: cellClass.reuseIdentifier())
    }
    
    public func dequeueCell<T: ReuseIdetifiable>(cellClass: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: cellClass.reuseIdentifier(), for: indexPath) as? T else {
            fatalError("Cell for \(indexPath) not \(NSStringFromClass(cellClass.self))")
        }
        return cell
    }
}

public extension UICollectionView {
    public func registerNib<T: ReusableFromNib>(for cellClass: T.Type) {
        register(cellClass.nib(), forCellWithReuseIdentifier: cellClass.reuseIdentifier())
    }
    
    public func dequeueCell<T: ReuseIdetifiable>(cellClass: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: cellClass.reuseIdentifier(), for: indexPath) as? T else {
            fatalError("Cell for \(indexPath) not \(NSStringFromClass(cellClass.self))")
        }
        return cell
    }
}
