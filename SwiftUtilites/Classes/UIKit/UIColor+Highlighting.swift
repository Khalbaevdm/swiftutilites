//
//  UIColor+Highlighting.swift
//
//
//  Created by Dmitriy Khalbaev on 02/02/2018.
//

import UIKit

public extension UIColor {
    public var highlighted: UIColor {
        return scaledComponentHighlighted()
    }
    
    public var isLight: Bool {
        var white: CGFloat = 0
        getWhite(&white, alpha: nil)
        return white > 0.5
    }
}

public extension UIColor {
    public func scaledComponent(by scale: CGFloat) -> UIColor {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        
        var result: UIColor = self
        
        guard let colorSpace = cgColor.colorSpace else {
            return result
        }
        
        guard let colorComponents = cgColor.components else {
            return result
        }
        
        let colorSpaceModel = colorSpace.model
        let colorComponentsCount = cgColor.numberOfComponents
        
        switch colorSpaceModel {
        case .monochrome:
            if colorComponentsCount == 2 {
                red = colorComponents[0]
                green = colorComponents[0]
                blue = colorComponents[0]
                alpha = colorComponents[1]
                
                result = UIColor(red: red * scale,
                                 green: green * scale,
                                 blue: blue * scale,
                                 alpha: alpha)
            }
        case .rgb:
            if colorComponentsCount == 4 {
                red = colorComponents[0]
                green = colorComponents[1]
                blue = colorComponents[2]
                alpha = colorComponents[3]
                
                result = UIColor(red: red * scale,
                                 green: green * scale,
                                 blue: blue * scale,
                                 alpha: alpha)
            }
        default:
            break
        }
        
        return result
    }
    
    public func scaledComponentHighlighted() -> UIColor {
        return scaledComponent(by: 0.53)
    }
}
