//
//  UIImage+Highlight.swift
//
//
//  Created by Dmitriy Khalbaev on 26/03/2018.
//
//

import Foundation

public extension UIImage {
    public func highlight(by color: UIColor) -> UIImage {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        
        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
