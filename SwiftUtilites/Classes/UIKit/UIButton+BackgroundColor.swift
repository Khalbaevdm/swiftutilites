//
//  UIButton+BackgroundColor.swift
//
//
//  Created by Dmitriy Khalbaev on 02/03/2018.
//
//

import Foundation

public extension UIButton {
    public func setBackgroundColor(color: UIColor, for state: UIControl.State) {
        let image = UIImage(color: color)
        setBackgroundImage(image, for: state)
    }
}
