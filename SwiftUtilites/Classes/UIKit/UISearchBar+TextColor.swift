//
//  UISearchBar+TextColor.swift
//  VeloityVue
//
//  Created by Dmitriy Khalbaev on 30/05/2018.
//  Copyright © 2018 NTR Lab. All rights reserved.
//

import Foundation

public extension UISearchBar {
    public var textColor: UIColor? {
        set {
            textField?.textColor = newValue
        }
        get {
            return textField?.textColor
        }
    }
    
    public var textField: UITextField? {
        return getViewElement(type: UITextField.self)
    }
    
    private func getViewElement<T>(type: T.Type) -> T? {
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else {
            return nil
        }
        return element
    }
}
