//
//  Reactive+UINavigationController.swift
//  
//
//  Created by Dmitriy Khalbaev on 16/03/2018.
//
//

import ReactiveSwift
import ReactiveCocoa

extension Reactive where Base: UINavigationController {
    public func pushViewController(animated: Bool) -> BindingTarget<UIViewController> {
        return makeBindingTarget { $0.pushViewController($1, animated: animated) }
    }
    
    public func popViewController(animated: Bool) -> BindingTarget<()> {
        return makeBindingTarget({ navController, _ in
            navController.popViewController(animated: animated)})
    }
    
    public func popToRootViewController(animated: Bool) -> BindingTarget<()> {
        return makeBindingTarget({ navController, _ in
            navController.popToRootViewController(animated: animated)})
    }
}
