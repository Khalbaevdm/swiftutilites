//
//  ReactiveSwift+UIViewController.swift
//  
//
//  Created by Dmitriy Khalbaev on 06/02/2018.
//
//

import ReactiveSwift
import ReactiveCocoa

extension Reactive where Base: UIViewController {
    public func presentViewController(animated: Bool) -> BindingTarget<UIViewController> {
        return makeBindingTarget { $0.present($1, animated: animated) }
    }
    
    public func dismissViewController(animated: Bool) -> BindingTarget<()> {
        return makeBindingTarget { base, _ in base.dismiss(animated: true) }
    }
}
