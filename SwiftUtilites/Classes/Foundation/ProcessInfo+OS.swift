//
//  ProcessInfo+OS.swift
//  
//
//  Created by Dmitriy Khalbaev on 15/05/2018.
//
//

import Foundation

public extension ProcessInfo {
    public static let majorVersion: Int = {
        return ProcessInfo.processInfo.operatingSystemVersion.majorVersion
    }()
    
    public static let minorVersion: Int = {
        return ProcessInfo.processInfo.operatingSystemVersion.minorVersion
    }()
    
    public static let patchVersion: Int = {
        return ProcessInfo.processInfo.operatingSystemVersion.patchVersion
    }()
    
    public static let myOSVersion: String = {
        return ProcessInfo.processInfo.operatingSystemVersionString
    }()
}
