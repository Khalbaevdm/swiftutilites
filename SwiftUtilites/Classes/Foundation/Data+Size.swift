//
//  Data+Size.swift
//
//
//  Created by Dmitriy Khalbaev on 24/05/2018.
//
//

import Foundation

public extension Data {
    public enum SizeType {
        case bytes
        case kilobytes
        case megabytes
        
        public var divider: Double {
            let step: Double = 1024.0
            switch self {
            case .bytes:
                return 1
            case .kilobytes:
                return step
            case .megabytes:
                return step * step
            }
        }
    }
    
    public func size(in sizeType: SizeType) -> Double {
        let result = Double(count) / sizeType.divider
        return result
    }
}
