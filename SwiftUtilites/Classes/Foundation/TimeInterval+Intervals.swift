//
//  TimeInterval+Intervals.swift
//  
//
//  Created by Dmitriy Khalbaev on 14/02/2018.
//
//

import Foundation

public extension TimeInterval {
    static private let minuteInterval: TimeInterval = 60
    static private let hourInterval: TimeInterval = minuteInterval * 60
    static private let dayInterval: TimeInterval = hourInterval * 24
    static private let weekInterval: TimeInterval = dayInterval * 7
    
    public static func minutes(_ minutes: Int) -> TimeInterval {
        return minuteInterval * TimeInterval(minutes)
    }
    
    public static func hours(_ hours: Int) -> TimeInterval {
        return hourInterval * TimeInterval(hours)
    }
    
    public static func days(_ days: Int) -> TimeInterval {
        return dayInterval * TimeInterval(days)
    }
    
    public static func weeks(_ weeks: Int) -> TimeInterval {
        return weekInterval * TimeInterval(weeks)
    }
}
