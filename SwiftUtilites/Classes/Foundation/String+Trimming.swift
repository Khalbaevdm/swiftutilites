//
//  String+Trimming.swift
//
//
//  Created by Dmitriy Khalbaev on 29/05/2018.
//
//

import Foundation

public extension String {
    public var trailingSpacesTrimmed: String {
        var newString = self
        
        while newString.hasSuffix(" ") {
            newString = String(newString.dropLast())
        }
        
        return newString
    }
    
    public var leadingSpacesTrimmed: String {
        var newString = self
        
        while newString.hasPrefix(" ") {
            newString = String(newString.dropLast())
        }
        
        return newString
    }
}
