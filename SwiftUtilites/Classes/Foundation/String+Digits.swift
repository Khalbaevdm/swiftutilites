//
//  String+Digits.swift
//  
//
//  Created by Dmitriy Khalbaev on 26/03/2018.
//
//

import Foundation

public extension String {
    public var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
}
