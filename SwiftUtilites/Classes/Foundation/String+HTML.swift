//
//  String+HTML.swift
//  
//
//  Created by Dmitriy Khalbaev on 07/02/2018.
//
//

import Foundation

public extension String {
    public func convertHtml() -> NSAttributedString {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(
                data: data,
                options: [.documentType: NSAttributedString.DocumentType.html,
                          .characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
}
