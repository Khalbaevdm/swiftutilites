#
# Be sure to run `pod lib lint SwiftUtilites.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SwiftUtilites'
  s.version          = '0.1.12'
  s.summary          = 'SwiftUtilites for iOS applications.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
SwiftUtilites for iOS applications. Using ReactiveSwift. Contain Foundation utilites, UI utilites, Reactive utilites. Some base classes, like keyboard handler etc.
                       DESC

  s.homepage         = 'https://bitbucket.org/Khalbaevdm/swiftutilites'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Dmitriy Khalbaev' => 'dkhalbaev@dnomads.pro' }
  s.source           = { :git => 'https://bitbucket.org/Khalbaevdm/swiftutilites', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/KhalbaevDmitriy'

  s.ios.deployment_target = '9.0'
  s.swift_version = '4.2'
  # s.source_files = 'SwiftUtilites/Classes/**/*'

  s.subspec 'UIKit' do |ss|
      ss.source_files = 'SwiftUtilites/Classes/UIKit/**/*'
      ss.frameworks = 'UIKit'
  end

  s.subspec 'Foundation' do |ss|
      ss.source_files = 'SwiftUtilites/Classes/Foundation/*'
      ss.frameworks = 'Foundation'
  end

  s.subspec 'Classes' do |ss|
      ss.source_files = 'SwiftUtilites/Classes/Classes/*'
      ss.frameworks = 'UIKit'
  end

  s.subspec 'Reactive' do |ss|
      ss.source_files = 'SwiftUtilites/Classes/Reactive/*'
      ss.frameworks = 'UIKit'
      ss.dependency 'ReactiveSwift', '~> 5.0.0'
      ss.dependency 'ReactiveCocoa', '~> 9.0.0'
      ss.dependency 'Result', '~> 4.1'
  end

  # s.resource_bundles = {
  #   'SwiftUtilites' => ['SwiftUtilites/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'

end
