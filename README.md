# SwiftUtilites

[![CI Status](https://img.shields.io/travis/Dmitriy Khalbaev/SwiftUtilites.svg?style=flat)](https://travis-ci.org/Dmitriy Khalbaev/SwiftUtilites)
[![Version](https://img.shields.io/cocoapods/v/SwiftUtilites.svg?style=flat)](https://cocoapods.org/pods/SwiftUtilites)
[![License](https://img.shields.io/cocoapods/l/SwiftUtilites.svg?style=flat)](https://cocoapods.org/pods/SwiftUtilites)
[![Platform](https://img.shields.io/cocoapods/p/SwiftUtilites.svg?style=flat)](https://cocoapods.org/pods/SwiftUtilites)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SwiftUtilites is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SwiftUtilites'
```

## Author

Dmitriy Khalbaev, dkhalbaev@sinergo.ru

## License

SwiftUtilites is available under the MIT license. See the LICENSE file for more info.
